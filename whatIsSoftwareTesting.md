# What is Software Testing/ why testing is important

In essence: *"Software Testing is a process used to identify the correctness and quality of developed computer software. It includes a set of activities conducted with the intent of finding errors in software so that it could be corrected before the product is released to the end users."*

## Why is testing important
In the modern day world. People are heavily reliant on machines and software for their day-to-day living. For example:
- Modern cars use software for functionalities for road awareness e.g. parking sensors.
- Airplanes use software to get the plane to work properly.
- Healthcare machines used to diagnose and treat peoples illness use software to function.

Flaws within a software are called bugs. Software bugs can potentially cause monitary or even loss of life. For specific study cases as to how bugs can be costly click [here](https://raygun.com/blog/costly-software-errors-history/). 

## Seven Testing Principles: Software Testing
Software testing can be quite a complex and a resource-intensive process. Because of this, you will need some structured process so that you are able to get the best qualty on yourtesting investment. 
In comes the seven principles of testing. The seven principles of testing help you to set optimum quality standards and give you and your customers the confidence that your sotware is ready and working.

### So what are the seven principles?
1. **Principle 1:** Testing shows the presence of defects, not their absence.
What this could mean is that, if your company has a QA team and that team has reported that there are no bugs in the software. This doesn't actually mean that their are no bugs. This means that their could be bugs but the QA team wasn't able to find them.

2. **Principle 2:** Exhaustive testing is impossible.
Here's a scenario: You have 15 input fields to test and each input fields has about 10 possible values that could be inserted into the field. The number of combinations that would need to be tested would be 10 to the powere of 15. 
To test all the possible combinations. The project Execution time an cost would rise exponentially.

3. **Principle 3:** Early testing saves time and money.
This principle is pretty much self-explanatory. By quickly revealing flaws, this can save you from massive delays down the line. and as they say: "*time is money*".

4. **Principle 4:** Defects cluster together.
This principle is an example of the 80:20 rule. What the 80:20 rule is, 80 percent of defects are due to 20 percent of code. This is about focusing on the right area.

5. **Principle 5:** Beware of the pesticide paradox.
The pesticide paradox is based on the use of pesticide on pests makes them immune to its use. In much the same way, subjecting a module to the the same test cases can make a module immune to the test cases.

6. **Principle 6:** Testing is context-dependent.
Knowing that there isn't a single strategy that works for everything. To come up with the best strategy depends on the context of what you're testing.

7. **Principle 7:** Beware of the absence-of-errors fallacy.
There is a common misconception that lack of bugs/errors found shows that the product is a success. This is not the case. If the software doesn't meet the needs of the client, it will not make it a success. 


## Testing in a life cycle models
There are numerous development life cycle models.
Development model selected for a project, depends on the aims and goals of that project.
Testing is a stand-alone activity and it has to adopt with the development model chosen for the project.
In any model, testing should be performed at all levels.

## What is unit testing?
Unit testing is also known as component testing.
It is performed on standalone module to check whether it is developed correctly.

## What is System Testing?
System testing is concerned with behaviour of the system as a whole.
Unlike Intergration Testing, which focuses on data transfer amongst modules. System testing checks complete end to end scenarios, as the way the customer would use the system.

## What is Acceptance Testing?
Acceptance test is usually done at client location by the client.
Focus of Acceptance test is not to find defects but to check whether the system meets their requirements. 

## What is integration Testing?
In this phase of testing, individual modules are combined and tested as a group.
Data transfer between the modules is tested thoroughly.
integration testing is carried out by testers.

## Types of Testing:

There are 3 types of testing. Within those types of testing there are various methods:

|Functional | Non-Functional | Maintenance |
|-----------|----------------|-------------|
| Unit Testing | Performance | Regression |
| Integration Testing | Endurance | Maintenance |
| Smoke / Sanity | Load |    |
| User Acceptance | Volume |    |
| Localisation | Scalability |    |
| Gloablisation | Usability |    |
| Interoperability | So on... |   |
| So On ... |    |    |
