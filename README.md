# How The Internet Works

The internet is not a cloud as most people are led to believe. The internet is actually a connection of wires. This could be through connections such as:
- Fibre optic cables
- Copper cables
- Satellite connections
- Mobile networks

## Components of the internet
The internet is made up of many devices. For instance, you have servers which are directly connected to the internet. Servers basically computers that are mainly used to host data, this could be a website, an app etc. These data are stored on the hard drive of that server. 

Then you have devices that connect to those servers. This could be a PC, a tablet, a gaming console and the list goes on. In order for these devices to communicate with the server, the servers all need an IP address. The IP address works in much the same way as how your postal address works. It is entirely unique and is used to find a specific device on the internet. an example of an IP address is: ==72.14.205.100==. Because the human brain isn't built to remember numbers easily, these IP addresses are translated into our language that we can understand. So `72.14.205.100` can be represented as 'Google.com'. So if Bob types in *Google.com* He is then directed to the server that hosts the Google.com website. The name that is assigned to the IP address is handled by what is known as a DNS. What DNS stands for is Domain Name System. A basic explanation of this is, the phone book for IP addresses. 

The devices/computers we use on the internet are called clients. This is because out devices to connect directly to the internet. We gain our internet through organisations known as Internet Service Providers (ISP for short). An example of an ISP is KCOM. KCOM has the infrasture ~~incapable~~ capable of connecting us to the internet and allowing us access to the internet. The client is also assigned an IP address which is needed to join the internet. So when a client wants to send and retrieve data. The data is broken down into pieces known as packets and then is reassembled at the destination in order to make sense of the data. 

## Putting the pieces together
So the way it all this works is like this: for a basic example of how the internet works. On one end you have the client, and on the opposite end you have a server. If the client wanted to view the website of *Google.com*, the client would need to make sure they have an internet connection from whatever device they're using. For this example we'll use a laptop.
The client types in their browser, "Google.com". This request travels from their Laptop through WiFi or Ethernet to their router. From the router, it then sends this data to a small green boxes you see relatively often out in the streets near your home. This is done either through copper cables or fibre optic cables, the latter being the fastest. From there it is sent to the ISP. From the ISP out onto the internet and from the internet to another router which connects to the google server. Along the way of sending the request. Each router along the way will wrap the data given to it with its own meta data of source and destination IP in order for the data to find its way home to the device that requested it in the first place. Sort of like a road-map for the data to travel. The server will then receive this request and send the website data exactly the same way the request came through for that website.
