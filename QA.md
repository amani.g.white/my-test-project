These are my notes on the treehouse course for QA Engineering. 

# QA Engineering

## What is Quality Assurance Engineering?
There are 2 parts to this
1. QA: Is the method of preventing mistakes and defects from occuring in a product. A bit like quality control, but quality control is only a small aspect of QA.
2. SQA: Also known as 'Software Quality control', this is being involved in every step of the development process, coming up with ways to make sure that the software does what it is intended to do and to test the end product for bugs.

## Acceptance Criteria
Acceptance Critteria is a formal set of rules that defines how a feature must work. Acceptance criteria is usually defined in the planning phase. 

## The flow of the development process.
The development process goes along the lines of:
Idea -> Feature -> Development -> Test -> Release -> Feadback.

## Test Case
The test case is a series of steps that will attempt to prove the software us running to our specification. The test case acts almost like a journal of tests. 

**Below is an example blank Test Case:**

**BLANK TEST CASE**

	Name:

	Setup:

	Steps:

	| # | Steps | Expected Results |
	|---|-------|------------------|
	| 1 |  |  |

	Teardown:

## Happy Path vs. Testing Edge cases
**Happy Path:** This is basically testing with only the acceptance criteria of the feature. In other words what should work is tested. 

**Edge Cases:** This is basically testing outside of the based assumptions, finding ways to use a feature that were not intended.

## Creating a test plan
### Test plan topics:
- Scope of testing
- Schedule
- Test Deliverables
- Release Criteria
- Risks and contingencies

## Regression and Configuration testing
**Regression Testing:**
Before a new software is released, we want to know whether the old features that have already been released and works, are still working. To do this we will need to test what has been released in the past along side the new features. In other words, making sure what used to work is still working.

**Configuration Testing:**
This is testing all the systems in their configurations that your software should run on reliably. Here's an example of a configuration test:

| OS and Browser | Windows 10 | Windows 8 | OSX - High Sierra |
|----------------|------------|-----------|-------------------|
| Chrome | Pass | Pass | Pass |
| FireFox | Pass | Pass | pass |
| Safari |  |  | Pass |
| Edge | Pass | Fail |  |

## Exploration Testing and Automation
**Exploration Testing:**
A brief summary is exploring the application casually with certain things in mind to look out for. It should be done within a specific period of time and see what you can find.

**Automation:**
For repetitive tasks such as certain regression testing, you can put these into code and have your machine carry out the work and turn what can take hours to carry out into minutes worth of work.

## What is a Bug or a Defect?
Generally speaking, a bug/defect is a flaw in the software that must be fixed.

## Writing a bug report
Here is an example outline of bug reporting:

Name/Description:

Location:

Screenshots/Videos:

Steps to Reproduce:

1. --
2. --
3. --

Expected Results:

Actual Result:

Data set:

User:

Workaround:

## Severity vs. Priority
Severity: *A rigidly defined set of criteria for how bad a bug is to the system. Ranged High, Medium, Low.*

Priority: *How quickly the bug needs to be fixed. Ranged High to low as well*

//Note to self - A Terminology to keep in mind is Defect backlog. This is a list of defects that are not currently assigned to be worked on, but are known to the company and could easily be pulled in when there is extra time.

## Black Box Testing vs. White Box Testing
Black box testing is when a tester with no knowledge of the underlying code and only testsaccording to the desired user experience. 

White box is a tester with intimate knowledge of the underlying code and allows that to inform them where to test.

## Testing Charts
**Load Test Chart**: How quickly does the server respond to a certain amount of users? Usually this test will measure responsiveness, then add more users and measure responsiveness again.

**Stress Test**: How many users can the server handle before it doesn't respond at all? This test continues to add more users until the server shuts down entirely.

**Soak Test**: Can the server handle a load of users for a long period of time> This test puts a certain amount of users on the server for a period of several days to measure reliability.
